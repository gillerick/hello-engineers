import 'package:flutter/material.dart';



void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("Hello Engineers"),
          backgroundColor: Colors.blueGrey,
        ),
        body: ListView(
          children: <Widget>[
            ListTile(
              leading: Icon(Icons.person, color: Colors.orangeAccent, size: 32,),
              title: Text('Gill Erick'),
              subtitle: Text('2021 • April'),
            ),
            ListTile(
              leading: Icon(Icons.person, color: Colors.blue, size: 32,),
              title: Text('Dorcas Cherono'),
              subtitle: Text('2021 • April'),
            ),
            ListTile(
              leading: Icon(Icons.person, color: Colors.deepPurple, size: 32,),
              title: Text('Maxwell Kiprop'),
              subtitle: Text('2021 • April'),
            ),
            ListTile(
              leading: Icon(Icons.person, color: Colors.redAccent, size: 32),
              title: Text('Stacey Chebet'),
              subtitle: Text('2021 • April'),
            ),
          ],
        ),
      ),
      debugShowCheckedModeBanner: false,
    );
  }
}
