# Hello Engineers

A simple Flutter application that displays a list of Sendy Interns' names, the year of the cohort and the month they joined.

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

## Demos
![Launch Screen](resources/splash_screen.jpeg)

Launch Screen

![Main Screen](resources/main_screen.jpeg)

Main Screen

![Application Logo](resources/logo.jpg)

Application Logo

## Key Learning Areas
- Learnt adding an image resource to a Flutter application using Android Studio
- Learnt to a splash screen
- Learnt to add a List view
- Gained a better understanding of Flutter logical widget tree
